package com.atlassian.pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This Page object holds the element identifiers and utility methods for the
 * Jira home page for both loggedIn and logged out use cases
 */
public class JiraHomePage {

	@FindBy(how = How.XPATH, using = "//li[@id='user-options']/a[text()='Log In']")
	private WebElement logInLink;

	private WebElement username;

	private WebElement password;

	@FindBy(how = How.ID, using = "loginbutton")
	private WebElement loginButton;

	private WebElement rememberMe;

	@FindBy(how = How.ID, using = "create_link")
	private WebElement createIssueButton;

	private WebElement searchString;

	public void loginToJira(WebDriver driver) {
		logInLink.click();
		username.sendKeys("sankarbhavanib@gmail.com");
		password.sendKeys("Password123");
		if (rememberMe.getAttribute("value").equals("true")) {
			rememberMe.click();
		}
		loginButton.click();

		waitUntilVisible(driver,createIssueButton);
	}

	public void invokeJiraUrl(WebDriver driver) {
		// Navigate to Jira home page
		driver.get("https://jira.atlassian.com/browse/TST");
	}

	public void clickCreateIssue() {
		createIssueButton.click();
	}

	public void searchIssue(WebDriver driver, String issueNumber) {
		waitUntilVisible(driver,searchString);

		searchString.sendKeys(issueNumber);
		searchString.sendKeys(Keys.ENTER);
	}
	
	private void waitUntilVisible(WebDriver driver, WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

}
