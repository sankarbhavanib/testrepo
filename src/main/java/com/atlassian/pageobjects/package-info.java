/**
 * This package contains the page objects for different pages needed to navigate the tests
 */
package com.atlassian.pageobjects;