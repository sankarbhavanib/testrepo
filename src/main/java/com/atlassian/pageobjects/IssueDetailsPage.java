package com.atlassian.pageobjects;

import java.util.Calendar;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This Page object holds the element identifiers and utility methods for the
 * "Issue view and/or edit" activity
 */

public class IssueDetailsPage {

	@FindBy(how = How.ID, using = "summary-val")
	private WebElement summary;

	@FindBy(how = How.ID, using = "reporter-val")
	private WebElement reporter;

	@FindBy(how = How.ID, using = "edit-issue")
	private WebElement editIssue;

	@FindBy(how = How.ID, using = "summary")
	private WebElement summaryEditable;

	public String getSummary(WebDriver driver) {
		waitUntilVisible(driver, summary);
		return summary.getText();
	}

	public String getReporterInfo(WebDriver driver) {
		waitUntilVisible(driver, reporter);
		return reporter.getText();
	}

	public void updateIssue(WebDriver driver) {
		waitUntilVisible(driver, editIssue);
		editIssue.click();

		waitUntilVisible(driver, summaryEditable);
		summaryEditable.clear();
		summaryEditable.sendKeys("New summary at "
				+ Calendar.getInstance().getTime());
		summaryEditable.sendKeys(Keys.ENTER);

		waitUntilVisible(driver, summary);
		driver.navigate().refresh();

	}

	private void waitUntilVisible(WebDriver driver, WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

}
