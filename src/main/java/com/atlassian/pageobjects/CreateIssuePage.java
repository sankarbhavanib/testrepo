package com.atlassian.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This Page object holds the element identifiers and utility methods for the
 * "Create New Issue" activity
 */
public class CreateIssuePage {

	@FindBy(how = How.ID, using = "project-field")
	private WebElement projectName;

	@FindBy(how = How.ID, using = "issuetype-field")
	private WebElement issueType;

	private WebElement summary;

	@FindBy(how = How.ID, using = "create-issue-submit")
	private WebElement createIssueButton;

	@FindBy(how = How.XPATH, using = "//div[@class='global-msg']//a[@class='issue-created-key issue-link']")
	private WebElement createdIssueId;

	public void createIssueAndSubmit(WebDriver driver) {
		summary.sendKeys("sample issue creation");
		createIssueButton.click();
	}

	public String getProjectName(WebDriver driver) {
		waitUntilVisible(driver, projectName);
		return projectName.getAttribute("value");
	}

	public String getCreatedIssueId(WebDriver driver) {
		waitUntilVisible(driver, createdIssueId);
		return createdIssueId.getAttribute("data-issue-key");
	}

	
	private void waitUntilVisible(WebDriver driver, WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
}
