package com.atlassian.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.atlassian.pageobjects.CreateIssuePage;
import com.atlassian.pageobjects.JiraHomePage;

/**
 * Use Case: New issues can be created
 */
public class CreateJiraIssue {

	@Test
	public void testIssueCreation() {
		// Create a new instance of a driver
		WebDriver driver = new FirefoxDriver();

		
		JiraHomePage jiraPage = PageFactory.initElements(driver,
				JiraHomePage.class);
		CreateIssuePage issuePage = PageFactory.initElements(driver,
				CreateIssuePage.class);

		//invoke the new url
		jiraPage.invokeJiraUrl(driver);
		
		//login to jira application
		jiraPage.loginToJira(driver);
		
		//initiate issue creation
		jiraPage.clickCreateIssue();
		
		Assert.assertEquals(issuePage.getProjectName(driver), "A Test Project",
				"Failed to verify that the issue is being created for right project");
		
		//create issue and verify creation
		issuePage.createIssueAndSubmit(driver);

		String issueID = issuePage.getCreatedIssueId(driver);
		Assert.assertTrue(!issueID.isEmpty() && issueID.startsWith("TST-"),
				"Failed to verify the issue creation");
		driver.quit();
	}

}
