package com.atlassian.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.atlassian.pageobjects.IssueDetailsPage;
import com.atlassian.pageobjects.JiraHomePage;

/**
 * Use Case: Existing issues can be updated
 */
public class UpdateJiraIssue {

	@Test
	public void testUpdateIssue() {
		// Create a new instance of a driver
		WebDriver driver = new FirefoxDriver();

		
		JiraHomePage jiraPage = PageFactory.initElements(driver,
				JiraHomePage.class);
		IssueDetailsPage detailsPage = PageFactory.initElements(driver,
				IssueDetailsPage.class);
		

		//login to jira application and search for an existing issue
		jiraPage.invokeJiraUrl(driver);
		jiraPage.loginToJira(driver);
		jiraPage.searchIssue(driver, "TST-53994");

		String issueSummaryBeforeUpdate = detailsPage.getSummary(driver);
		
		//update the issue and verify the details
		detailsPage.updateIssue(driver);

		String issueSummaryAfterUpdate = detailsPage.getSummary(driver);
		Assert.assertNotEquals(issueSummaryBeforeUpdate,
				issueSummaryAfterUpdate, "Validate the updates");

		Assert.assertEquals(detailsPage.getReporterInfo(driver),
				"Bhavani Bheemanadham",
				"Reporter verificaiton in issue details");
		driver.quit();
	}

}
