package com.atlassian.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.atlassian.pageobjects.IssueDetailsPage;
import com.atlassian.pageobjects.JiraHomePage;

/**
 * Use Case: Existing issues can be found via JIRA�s search
 */
public class SearchJiraIssue {

	@Test
	public void testSeatchIssue() {
		// Create a new instance of a driver
		WebDriver driver = new FirefoxDriver();

		JiraHomePage jiraPage = PageFactory.initElements(driver,
				JiraHomePage.class);
		IssueDetailsPage detailsPage = PageFactory.initElements(driver,
				IssueDetailsPage.class);
		
		//invoke the website address to login 
		jiraPage.invokeJiraUrl(driver);
		
		//search for an existing issue without logging in
		jiraPage.searchIssue(driver, "TST-53997");
		
		//verify the issue details
		Assert.assertEquals(detailsPage.getSummary(driver),
				"sample issue creation",
				"Summary verificaiton in issue details");

		Assert.assertEquals(detailsPage.getReporterInfo(driver),
				"Bhavani Bheemanadham",
				"Reporter verificaiton in issue details");
		driver.quit();
	}

}
