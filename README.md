Created a maven test project which will help to test the following usecases:

 1. New issues can be created
 2. Existing issues can be updated
 3. Existing issues can be found via JIRAs search

Environment:
Java 6, Maven 2.2.1, TestNG 6.8.8, Selenium 2.41.0

Assumptions:
1. The test execution is done using firefox browser 
2. If tests are executed through IDE, testNG plugin is installed and/or if tests are executed from commandline testNG executable is available
Useful link for execution process: http://testng.org/doc/documentation-main.html#running-testng


Suite file is created in case to execute all the test scenarios. Location: src\test\resources\ExecutionSuite.xml